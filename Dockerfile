FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/demo-rest-service-0.1.0.jar target/progreg-app.jar
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=local","target/progreg-app.jar"]